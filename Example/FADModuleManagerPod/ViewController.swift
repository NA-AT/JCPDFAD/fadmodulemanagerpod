//
//  ViewController.swift
//  FADModuleManagerPod
//
//  Created by jcjobs on 02/07/2019.
//  Copyright (c) 2019 jcjobs. All rights reserved.
//

import UIKit
import FADModuleManagerPod

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
         let constants : FADConstants  = FADConstants()
        
        FADModuleManager.initProccess(navigation: self.navigationController, constants: constants, onSuccess: { () in
            print("onSuccess")
        }, onCancel: {
            print("onCancel")
        }) {
            print()
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}

