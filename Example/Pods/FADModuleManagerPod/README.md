# FADModuleManagerPod

[![CI Status](https://img.shields.io/travis/jcjobs/FADModuleManagerPod.svg?style=flat)](https://travis-ci.org/jcjobs/FADModuleManagerPod)
[![Version](https://img.shields.io/cocoapods/v/FADModuleManagerPod.svg?style=flat)](https://cocoapods.org/pods/FADModuleManagerPod)
[![License](https://img.shields.io/cocoapods/l/FADModuleManagerPod.svg?style=flat)](https://cocoapods.org/pods/FADModuleManagerPod)
[![Platform](https://img.shields.io/cocoapods/p/FADModuleManagerPod.svg?style=flat)](https://cocoapods.org/pods/FADModuleManagerPod)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

FADModuleManagerPod is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'FADModuleManagerPod'
```

## Author

jcjobs, jcperez@na-at.com.mx

## License

FADModuleManagerPod is available under the MIT license. See the LICENSE file for more info.
