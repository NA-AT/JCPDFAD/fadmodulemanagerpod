//
//  Extensions.swift
//  FADModuleSelfiePod
//
//  Created by Juan Carlos Pérez on 1/24/19.
//

import UIKit

class Extensions: NSObject {

}


// MARK: - String extension, localized strings
extension String {
    
    //JCPD
    var localized: String {
        
        //let bundle = Bundle(identifier: "com.na-at.FADModule")
        //let bundle = Bundle(for: FADSelfieViewController.self)
        
        
        let podBundle = Bundle(for: FADMainModuleViewController.self)
        let bundleURL = podBundle.url(forResource: "FADModuleManagerPod", withExtension: "bundle")
        let bundle = Bundle(url: bundleURL!)!
        
        
        return NSLocalizedString(self, tableName: nil, bundle: bundle, value: "", comment: "")
    }
    
    
}


public extension UIFont {
    public static func register(from url: URL) throws {
        guard let fontDataProvider = CGDataProvider(url: url as CFURL) else {
            // throw SVError.internal("Could not create font data provider for \(url).")
            return
        }
        let font = CGFont(fontDataProvider)
        var error: Unmanaged<CFError>?
        guard CTFontManagerRegisterGraphicsFont(font!, &error) else {
            throw error!.takeUnretainedValue()
        }
    }
}
