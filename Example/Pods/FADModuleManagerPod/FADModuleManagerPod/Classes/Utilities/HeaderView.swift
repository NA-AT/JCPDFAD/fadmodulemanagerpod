//
//  CardView.swift
//  CICashMulticurrency
//
//  Created by Juan Carlos Pérez D. on 2/7/18.
//  Copyright © 2018 Juan Carlos Pérez D. All rights reserved.
//


import UIKit

@IBDesignable
class HeaderView: UIView {
    
    @IBInspectable var cornerRadius: CGFloat = 20
    @IBInspectable var height: CGFloat = 10
    @IBInspectable var shadowColor: UIColor? = UIColor.black
    
    override func layoutSubviews() {
        let border = CALayer()
        border.backgroundColor = shadowColor?.cgColor
        border.frame = CGRect(x: 0, y: frame.size.height - height, width: frame.size.width, height: height)
        layer.addSublayer(border)
    }
    
}
