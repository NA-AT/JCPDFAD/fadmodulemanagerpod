//
//  UIConstants.swift
//  FAD
//
//  Created by Eduardo Martinez Calderòn on 31/05/17.
//  Copyright © 2017 NA-AT. All rights reserved.
//

import UIKit

class UIConstants: NSObject {
    
    /// Fonts
    static let ALERTTITLE = "RobotoCondensed-Regular"  //"RobotoCondensed-Regular"
    static let ALERTNAME = "SourceSansPro-Semibold"   //"SourceSansPro-Semibold"
    static let ALERTTEXT = "SourceSansPro-Light"   //"SourceSansPro-Light"
    
    /// Size
    static let ALERTTITLESIZE: CGFloat = 18
    static let ALERTNAMESIZE: CGFloat = 15
    static let ALERTTEXTSIZE: CGFloat = 14
    
    /// Colors
    static let BACKGROUNDCOLOR = 0xF3F3F4
    static var INTERACTIONCOLOR = 0xEB0029//0xED6811
    static var SECONDARYCOLOR = 0x4A4A4A
    static let INACTIVECOLOR = 0xA2A6AC
    
    static let TEXT_FIELD_UNDERLINE_COLOR = 0x979797
    static let TEXT_FIELD_UNDERLINE_ERROR_COLOR = 0xcc2e2e
    static let AMEXINTERACTIONCOLOR = 0x009DC0
    
    //BANORTE
    static let BGRAY = 0x666666
    static let BLIGTHGRAY = 0xE1E1E1
    static let BBLACK = 0x4E4044
    static var BRED = 0xEB0029
    static var BYELLOW = 0xFFA500
    
    
    

}
