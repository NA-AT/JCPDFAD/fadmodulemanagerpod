//
//  FADModulePhoto.h
//  FADModulePhoto
//
//  Created by Juan Carlos Pérez on 1/23/19.
//  Copyright © 2019 Juan Carlos Pérez. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for FADModulePhoto.
FOUNDATION_EXPORT double FADModulePhotoVersionNumber;

//! Project version string for FADModulePhoto.
FOUNDATION_EXPORT const unsigned char FADModulePhotoVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <FADModulePhoto/PublicHeader.h>


#import "OpenCVManager.h"
