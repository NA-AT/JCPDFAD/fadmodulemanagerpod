//
//  SignaturePoint.swift
//  FADModulePhoto
//
//  Created by Juan Carlos Pérez on 1/23/19.
//  Copyright © 2019 Juan Carlos Pérez. All rights reserved.
//

import UIKit

class SignaturePoint: NSObject {
    var x : String = ""
    var y : String = ""
    var timeStamp : Double = 0
}
