//
//  Singleton.swift
//  FAD
//
//  Created by Eduardo Martinez Calderòn on 30/05/17.
//  Copyright © 2017 NA-AT. All rights reserved.
//

import Foundation
//import Firebase


///Singleton Class
class Singleton : NSObject {
    
    
    /// holds the current user
    var user = "fad"
    /// holds the user password
    var password = "fadsecret"
    
    var imgSelfie : UIImage?
    
    var interfaceSign: UIInterfaceOrientation = .landscapeRight
    
    
    /// Function that creates singleton instance
    static let sharedInstance : Singleton = {
        let instance = Singleton()
        return instance
    }()
    
    
}
