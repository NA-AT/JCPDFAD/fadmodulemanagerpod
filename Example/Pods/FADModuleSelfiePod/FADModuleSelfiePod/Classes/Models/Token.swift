//
//  Token.swift
//  FADModulePhoto
//
//  Created by Juan Carlos Pérez on 1/23/19.
//  Copyright © 2019 Juan Carlos Pérez. All rights reserved.
//

import UIKit

class Token: NSObject {
    var accessToken = ""
    var expiresIn = 0
    var jti = ""
    var refreshToken = ""
    var scope = ""
    var tokenType = ""
}
