//
//  FADSelfieConstants.swift
//  FADModuleSelfiePod
//
//  Created by Juan Carlos Pérez on 1/28/19.
//

import UIKit

public class FADSelfieConstants: NSObject {
    
    open var imgSelfie : UIImage?
    
    open var isSaveProcess : Bool = false
    open var isProofLifeSelfie : Bool = false
    open var timeToTakeSelfie : Int = 0
    
    open var openEyesProbability : CGFloat = 0.85
    open var closeEyesProbability : CGFloat = 0.50
    open var smileProbability : CGFloat = 0.37
    
}
