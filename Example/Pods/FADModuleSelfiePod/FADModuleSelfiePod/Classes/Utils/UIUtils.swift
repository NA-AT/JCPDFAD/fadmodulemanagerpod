//
//  UIUtils.swift
//  FAD
//
//  Created by Eduardo Martinez Calderòn on 31/05/17.
//  Copyright © 2017 NA-AT. All rights reserved.
//

import UIKit

class UIUtils: NSObject {
  
    typealias completionBlock = () -> Void
    
    
    static func getBackgroundColor () -> UIColor{
        if let color = Constants.backGroundColor{
            return color
        }else{
            return UIColor.white
        }
    }
    
    /// Generic show alert method
    ///
    /// - Parameters:
    ///   - title: String, the title of the alert
    ///   - message: String, the message io the alert
    ///   - view: ViewController, the controller where the alert will show
    static func showAlert (texts : [String:String], view: UIViewController, resultFunc : @escaping completionBlock ) {
        
        let alert = UIAlertController(title: texts["title"], message: texts["message"], preferredStyle: .alert)
        alert.view.tintColor = UIColor(netHex:UIConstants.BRED)
        /*
         if Constants.isCUBAMEX{
         alert.view.tintColor = UIColor(netHex: UIConstants.AMEXINTERACTIONCOLOR)
         }else{
         alert.view.tintColor = UIColor(netHex: UIConstants.INTERACTIONCOLOR)
         }*/
        
        
        let titleFont: [String : AnyObject] = [convertFromNSAttributedStringKey(NSAttributedString.Key.font): UIFont(name:UIConstants.ALERTTITLE, size: CGFloat(UIConstants.ALERTTITLESIZE))!]
        let titleText = NSMutableAttributedString(string: texts["title"]!, attributes: convertToOptionalNSAttributedStringKeyDictionary(titleFont))
        
        let textFont: [String : AnyObject] = [convertFromNSAttributedStringKey(NSAttributedString.Key.font): UIFont(name: UIConstants.ALERTTEXT, size: CGFloat(UIConstants.ALERTTEXTSIZE))!]
        let text = NSMutableAttributedString(string: texts["message"]!, attributes : convertToOptionalNSAttributedStringKeyDictionary(textFont))
        
        let alertAction = UIAlertAction(title: texts["button"], style: .default) { (_) in
            resultFunc()
        }
        
        alert.setValue(titleText, forKey: "attributedTitle")
        alert.setValue(text, forKey: "attributedMessage")
        
        alert.addAction(alertAction)
        view.present(alert, animated: true, completion: nil)
        
    }
    
    
    /// Generic show alert method
    ///
    /// - Parameters:
    ///   - title: String, the title of the alert
    ///   - message: String, the message io the alert
    ///   - view: ViewController, the controller where the alert will show
    static func showAlertTwo (texts : [String:String], view: UIViewController, resultFuncCancel : @escaping completionBlock, resultFunc : @escaping completionBlock ) {
        
        let alert = UIAlertController(title: texts["title"], message: texts["message"], preferredStyle: .alert)
        alert.view.tintColor = UIColor(netHex:UIConstants.BRED)
        /*
         if Constants.isCUBAMEX{
         alert.view.tintColor = UIColor(netHex: UIConstants.AMEXINTERACTIONCOLOR)
         }else{
         alert.view.tintColor = UIColor(netHex: UIConstants.INTERACTIONCOLOR)
         }*/
        
        
        
        let titleFont: [String : AnyObject] = [convertFromNSAttributedStringKey(NSAttributedString.Key.font): UIFont(name:UIConstants.ALERTTITLE, size: CGFloat(UIConstants.ALERTTITLESIZE))!]
        let titleText = NSMutableAttributedString(string: texts["title"]!, attributes: convertToOptionalNSAttributedStringKeyDictionary(titleFont))
        
        let textFont: [String : AnyObject] = [convertFromNSAttributedStringKey(NSAttributedString.Key.font): UIFont(name: UIConstants.ALERTTEXT, size: CGFloat(UIConstants.ALERTTEXTSIZE))!]
        let text = NSMutableAttributedString(string: texts["message"]!, attributes : convertToOptionalNSAttributedStringKeyDictionary(textFont))
        
        var btnCancel = ""
        if texts["cancel"] == nil{
            btnCancel = "cancel".localized
        }else{
            btnCancel = texts["cancel"]!
        }
        
        
        let cancelAction = UIAlertAction(title: btnCancel, style: .destructive) { (_) in
            resultFuncCancel()
        }
        
        let action = UIAlertAction(title: texts["button"], style: .default) { (_) in
            resultFunc()
        }
        
        alert.setValue(titleText, forKey: "attributedTitle")
        alert.setValue(text, forKey: "attributedMessage")
        
        alert.addAction(cancelAction)
        alert.addAction(action)
        view.present(alert, animated: true, completion: nil)
        
    }
    

    /// Returns the attributed string of the message in the alert
    ///
    /// - Parameters:
    ///   - text: String, the text
    ///   - left: Alignment, left or center
    /// - Returns: NSMutableAttributedString, returns the attributed string
    static func getAlertText (text: String, left: Bool) -> NSMutableAttributedString {
        let titleAlignment = NSMutableParagraphStyle()
        
        if left {
            titleAlignment.alignment = NSTextAlignment.left
        } else {
            titleAlignment.alignment = NSTextAlignment.center
        }
        
        let textFont: [String : AnyObject] = [convertFromNSAttributedStringKey(NSAttributedString.Key.font): UIFont(name: UIConstants.ALERTTEXT, size: CGFloat(UIConstants.ALERTTEXTSIZE))!, convertFromNSAttributedStringKey(NSAttributedString.Key.paragraphStyle): titleAlignment]
        let text = NSMutableAttributedString(string: text, attributes: convertToOptionalNSAttributedStringKeyDictionary(textFont))
        return text
    }
    
    
    /// Returns the attributed string of the title
    ///
    /// - Parameters:
    ///   - title: String, the text of the title
    ///   - left: Alignment , left or center
    /// - Returns: NSMutableAttributedString, return the attributed string
    static func getAlertTitle (title: String, left: Bool) -> NSMutableAttributedString {
        
        let titleAlignment = NSMutableParagraphStyle()
        if left {
            titleAlignment.alignment = NSTextAlignment.left
        } else {
            titleAlignment.alignment = NSTextAlignment.center
        }
        
        let titleFont: [String : AnyObject] = [convertFromNSAttributedStringKey(NSAttributedString.Key.font): UIFont(name:UIConstants.ALERTTITLE, size: CGFloat(UIConstants.ALERTTITLESIZE))!, convertFromNSAttributedStringKey(NSAttributedString.Key.paragraphStyle): titleAlignment ]
        let titleText = NSMutableAttributedString(string: title, attributes: convertToOptionalNSAttributedStringKeyDictionary(titleFont))
        
        return titleText
    }

}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertFromNSAttributedStringKey(_ input: NSAttributedString.Key) -> String {
	return input.rawValue
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertToOptionalNSAttributedStringKeyDictionary(_ input: [String: Any]?) -> [NSAttributedString.Key: Any]? {
	guard let input = input else { return nil }
	return Dictionary(uniqueKeysWithValues: input.map { key, value in (NSAttributedString.Key(rawValue: key), value)})
}
