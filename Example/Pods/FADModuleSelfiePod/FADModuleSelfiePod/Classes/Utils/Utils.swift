//
//  Utils.swift
//  FAD
//
//  Created by Eduardo Martinez Calderòn on 30/05/17.
//  Copyright © 2017 NA-AT. All rights reserved.
//

import UIKit
import Foundation


class Utils: NSObject {
    
 
    /// Convert data to JSON dictionary
    ///
    /// - Parameter data: data to convert
    /// - Returns: NsDictinary , json dictionary
    static func convertDataToJSON(data: Data) -> NSDictionary {
        var json: NSDictionary = [:]
        do {
            if let json = try JSONSerialization.jsonObject(with: data, options: JSONSerialization.ReadingOptions.allowFragments) as? NSDictionary {
                return json
            }
        } catch {
            json = [:]
        }
        return json
    }
    
    
    static func getCompanyDynamicImage () -> UIImage{

        let image = getImageWithColor(color: UIColor.clear, size: CGSize(width: 1, height: 1))
        if Constants.imgCompany != nil{
            return Constants.imgCompany!
        }else{
            return image
        }
        //UIImage(named: "2000PxAmericanExpressLogoSvg")//
       
    }

    /// Return an image with the size and the color of the parameters
    ///
    /// - Parameters:ange
    ///   - color: Color
    ///   - size: size
    /// - Returns: return image
    static func getImageWithColor(color: UIColor, size: CGSize) -> UIImage {
        let rect = CGRect(x: 0, y: 0, width: size.width, height: size.height)
        UIGraphicsBeginImageContextWithOptions(size, false, 0)
        color.setFill()
        UIRectFill(rect)
        let image: UIImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        return image
    }
    
    //JCPD:
    static func getImageInBundle (image: String , viewController : UIViewController) -> UIImage?{
        
        //let budnles = Bundle.main.paths(forResourcesOfType: "bundle", inDirectory: nil)
        
        //let podBundle = Bundle(for: Utils.self)
        let podBundle = Bundle(for: type(of: viewController))
        let bundleURL = podBundle.url(forResource: "FADModuleSelfiePod", withExtension: "bundle")
        let bundle = Bundle(url: bundleURL!)!
        
        //let bundle = Bundle(identifier: "org.cocoapods.FADModuleSelfiePod")!

        return UIImage(named: image, in: bundle, compatibleWith: nil)
    }
    
    static func getAppState() -> UIApplication.State {
        let state : UIApplication.State = UIApplication.shared.applicationState
        return state
    }
    
    
    
    /// Returns the Documents Directory Path
    ///
    /// - Returns: String, the documents directory path
    static func getDocumentsDirectoryPath () -> String {
        let documentsPath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0]
        return documentsPath
        
    }
    
    
    /// Delete folder in Documents Directory
    ///
    /// - Parameter folder: String, Name of the folder to delete
    static func deleteFilesForDirectory(folder: String ) {
        let documentsPath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0]
        let filePath = documentsPath + "/\(folder)"
        let url = URL(fileURLWithPath: filePath)
        let fileManager = FileManager.default
        do {
            try fileManager.removeItem(at: url)
        } catch {
            print(error.localizedDescription)
        }
    }
    
    static func getSDKBundle () -> Bundle {
        //return Bundle(for: FADSelfieViewController.self)
        //return Bundle(identifier: bundleID)!
        //return Bundle(identifier: "com.na-at.FADModulePhoto")!
        
        
        let podBundle = Bundle(for: SelfieViewController.self)
        let bundleURL = podBundle.url(forResource: "FADModuleSelfiePod", withExtension: "bundle")
        let bundle = Bundle(url: bundleURL!)!
 
        
        //let bundle = Bundle(for: FADSelfieViewController.self)
        return bundle
    }
    
    static func fontsURLs() -> [URL] {
        let bundle = getSDKBundle()
        let fileNames = ["SourceSansPro-SemiboldItalic", "SourceSansPro-Semibold", "SourceSansPro-Regular","SourceSansPro-LightItalic","SourceSansPro-Light","SourceSansPro-Italic","SourceSansPro-ExtraLight","SourceSansPro-ExtraLightItalic","SourceSansPro-BoldItalic","SourceSansPro-Bold","SourceSansPro-BlackItalic","SourceSansPro-Black","RobotoCondensed-Regular","RobotoCondensed-LightItalic","RobotoCondensed-Light","RobotoCondensed-Italic","RobotoCondensed-BoldItalic","Roboto-Regular","Roboto-Light","Roboto-Thin","RobotoCondensed-Bold","Gotham-Bold","Gotham-Light","Gotham-Medium","GothamBook","Roboto-Medium","Roboto-Bold","Proxima-Nova-Bold", "Proxima-Nova-Regular", "Proxima-Nova-Semibold", "Proxima-Nova-Thin"]
        return fileNames.map({ bundle.url(forResource: $0, withExtension: "ttf")! })
    }
    
}
