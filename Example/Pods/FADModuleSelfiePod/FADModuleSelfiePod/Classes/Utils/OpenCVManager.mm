//
//  OpenCVManager.m
//  Firma_Autografa_Digital
//
//  Created by Eduardo Martinez Calderòn on 28/06/17.
//  Copyright © 2017 na-at. All rights reserved.
//

#import "OpenCVManager.h"
#import <opencv2/opencv.hpp>


using namespace cv;

@implementation OpenCVManager

+(int)checkLaplaceValue:(UIImage *)image{
    
    cv::Mat imgMat = [self cvMatFromUIImage:image];
    cv::Mat outputImg ;
    cv::Laplacian(imgMat, outputImg, CV_8U);
    cv::Mat laplacianImage;
    outputImg.convertTo(laplacianImage, CV_8UC1);
    
    unsigned char *pixels = laplacianImage.data;
    int maxLap = -16777216;
    
    for (int i = 0 ; i < laplacianImage.elemSize() * laplacianImage.total(); i ++){
        if (pixels[i] > maxLap){
            maxLap = pixels[i];
        }
    }
    
    //printf("%i",maxLap);
    return maxLap;

}

+(cv::Mat)cvMatFromUIImage:(UIImage *)image{
    CGColorSpaceRef colorSpace = CGImageGetColorSpace(image.CGImage);
    CGFloat cols = image.size.width;
    CGFloat rows = image.size.height;
    
    cv::Mat cvMat(rows,cols,CV_8UC4);
    
    CGContextRef contextRef = CGBitmapContextCreate(cvMat.data, cols, rows, 8, cvMat.step[0], colorSpace, kCGImageAlphaNoneSkipLast | kCGBitmapByteOrderDefault);
    CGContextDrawImage(contextRef, CGRectMake(0, 0, cols, rows), image.CGImage);
    CGContextRelease(contextRef);
    return cvMat;
}

+(UIImage *)resizeImage:(UIImage *)image targetSize:(CGSize)targetSize{
    
    cv::Mat imgMat = [self cvMatFromUIImage:image];
    
    cv::Mat outputImg ;
    
    cv::Size size(targetSize.width,targetSize.height);
    
    resize(imgMat, outputImg, size);
    
    UIImage *img = [self UIImageFromMat:outputImg];
    
    return img;
    
    
    
}



+ (UIImage *)UIImageFromMat:(cv::Mat)image

{
    
    
    
    cvtColor(image, image, CV_BGR2RGB);
    
    cvtColor(image, image, CV_RGB2BGR);
    
    NSData *data = [NSData dataWithBytes:image.data length:image.elemSize()*image.total()];
    
    
    
    CGColorSpaceRef colorSpace;
    
    
    
    if (image.elemSize() == 1) {
        
        colorSpace = CGColorSpaceCreateDeviceGray();
        
    } else {
        
        colorSpace = CGColorSpaceCreateDeviceRGB();
        
    }
    
    
    
    CGDataProviderRef provider = CGDataProviderCreateWithCFData((CFDataRef)data);//CGDataProviderCreateWithCFData((__bridge CFDataRef)data);
    
    
    
    // Creating CGImage from cv::Mat
    
    CGImageRef imageRef = CGImageCreate(image.cols,                                 //width
                                        
                                        image.rows,                                 //height
                                        
                                        8,                                          //bits per component
                                        
                                        8 * image.elemSize(),                       //bits per pixel
                                        
                                        image.step.p[0],                            //bytesPerRow
                                        
                                        colorSpace,                                 //colorspace
                                        
                                        kCGImageAlphaNone|kCGBitmapByteOrderDefault,// bitmap info
                                        
                                        provider,                                   //CGDataProviderRef
                                        
                                        NULL,                                       //decode
                                        
                                        false,                                      //should interpolate
                                        
                                        kCGRenderingIntentDefault                   //intent
                                        
                                        );
    
    
    
    
    
    // Getting UIImage from CGImage
    
    UIImage *finalImage = [UIImage imageWithCGImage:imageRef];
    
    //[self.imgView setImage:finalImage];
    
    CGImageRelease(imageRef);
    
    CGDataProviderRelease(provider);
    
    CGColorSpaceRelease(colorSpace);
    
    
    
    return finalImage;
    
}

@end
