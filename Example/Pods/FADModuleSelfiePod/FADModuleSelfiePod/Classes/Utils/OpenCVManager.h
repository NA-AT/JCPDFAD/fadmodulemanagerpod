//
//  OpenCVManager.h
//  Firma_Autografa_Digital
//
//  Created by Eduardo Martinez Calderòn on 28/06/17.
//  Copyright © 2017 na-at. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface OpenCVManager : NSObject

+(int)checkLaplaceValue:(UIImage *)image;
+(UIImage *)resizeImage:(UIImage *)image targetSize:(CGSize)targetSize;

@end
