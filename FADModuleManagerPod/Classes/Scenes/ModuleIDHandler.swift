//
//  ModuleIDHandler.swift
//  FADSelfieTest
//
//  Created by Juan Carlos Pérez on 2/6/19.
//  Copyright © 2019 Juan Carlos Pérez. All rights reserved.
//

import UIKit
import FADModuleIdPod

class ModuleIDHandler: NSObject, ProgressItemDelegate {
    var navController : UINavigationController?
    
    var delegate: FADManagerModuleDisplayLogic?

    
    func requestExecuteAction() {
        
        
        let constants = FADIdConstants()
        FADModuleIdPod.initProcess(navigation: navController!, constants: constants, onSuccess: { (results) in
            print("onSucces")
            let imgFront = results.imgIdFront
            let imgBack = results.imgIdBack
            self.navController?.popToRootViewController(animated: true)
        }, onCancel: {
            print("onCancel")
        }) {
            print("ONERROR")
        }

    }
}
