//
//  ModuleSelfieHandler.swift
//  FADSelfieTest
//
//  Created by Juan Carlos Pérez on 2/6/19.
//  Copyright © 2019 Juan Carlos Pérez. All rights reserved.
//

import UIKit
import FADModuleSelfiePod

class ModuleSelfieHandler: NSObject, ProgressItemDelegate {
    
    var navController : UINavigationController?
    var delegate: FADManagerModuleBusinessLogic?

    func requestExecuteAction() {
        
        let constants : FADSelfieConstants  = FADSelfieConstants()
        constants.isSaveProcess = Constants.isSaveProcess
        constants.isProofLifeSelfie = Constants.isProofLifeSelfie
        constants.timeToTakeSelfie =  Constants.timeToTakeSelfie
        constants.openEyesProbability = Constants.openEyesProbability
        constants.closeEyesProbability =  Constants.closeEyesProbability
        constants.smileProbability = Constants.smileProbability
        constants.imgSelfie = Constants.imgSelfie

      
        FADModulePhoto.initProccess(navigation: navController, constants: constants, onSuccess: {imgSelfie in
            print("onSucces")
            
            var selfieRequest =  FADManagerModule.Selfie.Request()
            selfieRequest.imgSelfie = imgSelfie
            self.delegate?.doSelfieProcess(request: selfieRequest)
            
        },onCancel: {
            print("onCancel")
           
        },onError: {
            print("onError")
           
        })
        
    }
    
}
