//
//  FADProgressViewItem.swift
//  FADSDKValidation
//
//  Created by Eduardo Martinez on 1/16/19.
//  Copyright © 2019 Na-at. All rights reserved.
//

import UIKit

protocol ProgressItemDelegate {
    func requestExecuteAction()
}

class FADProgressViewItem: UIView {

    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var btnCallModule: UIButton!
    
    var contentView: UIView?
    
    var delegate: ProgressItemDelegate! = nil
    
    enum ItemStatus {
        case inactive
        case active
        case inProgress
        case finished
    }
    
    var itemStatusType = ItemStatus.inactive
    

    override init(frame: CGRect) {
        super.init(frame: frame)
        xibSetup()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        xibSetup()
    }

    func loadViewFromNib() -> UIView {
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: String(describing: type(of: self)), bundle: bundle)
        guard let view = nib.instantiate(withOwner: self, options: nil).first as? UIView else {
            return UIView()
        }
        return view
    }

    func xibSetup() {
        contentView = loadViewFromNib()
        contentView?.frame = bounds
        contentView?.autoresizingMask = [UIView.AutoresizingMask.flexibleWidth, UIView.AutoresizingMask.flexibleHeight]
        addSubview(contentView!)
        
        self.updateStatus(status: .inactive)
    }
    
    func updateStatus(status : ItemStatus){
        self.itemStatusType = status
        
        switch itemStatusType {
        case .inactive:
            self.imgView.image = UIImage(named: "module_check_gray-icon")
            break
        case .inProgress:
            self.imgView.image = UIImage(named: "module_check_off-icon")
            break
        case .finished:
            self.imgView.image = UIImage(named: "module_check_on-icon")
            break
        default:
            //self.imgView.image = UIImage(named: "module_check_gray-icon")
            break
        }
    }
    
    
    @IBAction func callDelegate(_ sender : Any?){
        if self.itemStatusType == .inProgress ||  self.itemStatusType == .finished {
            if let progressView = self.superview as? FADProgressView {
                progressView.updateSubviewsStatus()
            }
            self.itemStatusType = .active
            
            delegate?.requestExecuteAction()
        }
    }

  
}
