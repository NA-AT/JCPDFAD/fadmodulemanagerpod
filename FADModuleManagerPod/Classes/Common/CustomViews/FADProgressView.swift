//
//  FADProgressView.swift
//  FADSelfieTest
//
//  Created by Juan Carlos Pérez on 2/6/19.
//  Copyright © 2019 Juan Carlos Pérez. All rights reserved.
//

import UIKit

class FADProgressView: UIStackView {

    func updateSubviewsStatus(){
        for itemView in self.subviews {
            if let progressItem  = itemView as? FADProgressViewItem {
                if progressItem.itemStatusType == .active{
                    progressItem.itemStatusType = .inProgress
                }
            }
        }
    }

}
