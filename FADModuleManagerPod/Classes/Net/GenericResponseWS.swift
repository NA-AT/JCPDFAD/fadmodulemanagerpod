//
//  GenericResponseWS.swift
//  GenericResponseWS
//
//  Created by Juan Carlos Pérez on 2/6/19.
//  Copyright © 2019 Juan Carlos Pérez. All rights reserved.
//

import UIKit

class GenericResponseWS: NSObject {

    var httpCode: Int
    var code: String
    var responseDescription: String
    var dataResponse : Data?
    
    convenience override init() {
        self.init(httpCode : 0, code:"", responseDescription: "", dataResponse : Data())
    }
    
    init(httpCode: Int, code: String, responseDescription: String, dataResponse: Data) {
        self.httpCode = httpCode
        self.code = code
        self.responseDescription = responseDescription
        self.dataResponse = dataResponse
    }

}
