//
//  ServiceConnector.swift
//  ServiceConnector
//
//  Created by Juan Carlos Pérez on 2/6/19.
//  Copyright © 2019 Juan Carlos Pérez. All rights reserved.
//


import UIKit

protocol ServiceConnectorDelegate{
    func requestReturnedData(_ responseObjectWS : GenericResponseWS)
}

class ServiceConnector: NSObject{
    
    enum RequestType {
        case post
        case get
        case put
        case delete
        case none
    }
    
    
    
    let SESSION_TRANSACTION_CANCELED = "500"
    let TIME_OUT_INTERVAL = 30.0
    
    var delegate:ServiceConnectorDelegate! = nil
    
    var urlString = "http://ww.host.com.mx".appending("/services/")
    
    var mutableData:NSMutableData?  = nil
    var responseObjectWS : GenericResponseWS? = nil
    
    var queue : OperationQueue? = nil
    
    var cancelled = Bool()
    
    
    func getQueryStringParameter(_ url: String, param: String) -> String? {
        guard let url = URLComponents(string: url) else { return nil }
        return url.queryItems?.first(where: { $0.name == param })?.value
    }
    
    func makeConnectionWithParametters(_ function: String, params: Dictionary<String, String>, requestType:RequestType){
        responseObjectWS = GenericResponseWS.init(httpCode: 503, code: "N", responseDescription: "No dispones de una conexión estable a Internet, intenta más tarde.", dataResponse: Data())
        
        /*
        //Verificar que se cuenta con acceso a internet
        guard Reachability.isConnectedToNetwork() else {
            DispatchQueue.main.async(execute: {
                self.delegate!.requestReturnedData(self.responseObjectWS!)
            })
            return
        }
        */
        
        var urlComp = URLComponents(string: urlString.appending(function) )!
  
        
        var theRequest = URLRequest(url: urlComp.url!)
        //let theRequest = NSMutableURLRequest(url: url)
        theRequest.timeoutInterval = TIME_OUT_INTERVAL
       
       
        //theRequest.addValue(CUserSession.sharedInstance.sessionToken, forHTTPHeaderField: "x-auth-token")//Token de la sesión
        let defaults = UserDefaults.standard
        if let token = defaults.string(forKey: "x-auth-token"){
            theRequest.addValue(token, forHTTPHeaderField: "x-auth-token")//Token de la sesión
        }
        
        //let langStr = Locale.current.languageCode//Device lenguaje
        let lng = Locale.preferredLanguages[0]//App lenguaje
        let arr = lng.components(separatedBy: "-")
        if let deviceLanguage = arr.first{
            if(deviceLanguage == "es"){
                theRequest.addValue(deviceLanguage, forHTTPHeaderField: "lng")//Lenguaje
            }
            else{
                theRequest.addValue("en", forHTTPHeaderField: "lng")//en-US
            }
        }
        


        
        switch requestType {
        case .post:
             theRequest.httpMethod = "POST"
             
             theRequest.addValue("application/json; charset=utf-8", forHTTPHeaderField: "Content-Type")
             theRequest.addValue("application/json", forHTTPHeaderField: "Accept")
             
             guard let httpBody = try? JSONSerialization.data(withJSONObject: params, options: []) else {
                return
             }
             theRequest.httpBody = httpBody
            break
        case .get:
             theRequest.httpMethod = "GET"
             //Agregar parámetros a la URL cuando es petición GET
             var items = [URLQueryItem]()
             for (key,value) in params {
                items.append(URLQueryItem(name: key, value: value))
             }
             items = items.filter{!$0.name.isEmpty}
             
             if !items.isEmpty {
                urlComp.queryItems = items
             }
             
            break
        default:
            break
        }
        
    
        // set up the session
        //let config = URLSessionConfiguration.default
        //let session = URLSession(configuration: config)
        let session = URLSession.shared
        let task = session.dataTask(with: theRequest as URLRequest, completionHandler: { data, response, error in

            //DLog("data: \(String(describing: data))")
            //DLog("response: \(String(describing: response))")
            //DLog("error: \(String(describing: error))")
            
            if let httpResponse  = response as! HTTPURLResponse?{
                
                //Obtener el token del header
                if let xAuthToken = httpResponse.allHeaderFields["x-auth-token"] as? String {
                    //xAuthToken
                    let defaults = UserDefaults.standard
                    defaults.set(nil, forKey: "x-auth-token")
                    defaults.set(xAuthToken, forKey: "x-auth-token")
                }
                
                self.responseObjectWS!.httpCode = httpResponse.statusCode
                
                switch(httpResponse.statusCode){
                case 200:
                   //Éxito
                    self.responseObjectWS!.dataResponse = data
                    self.responseObjectWS!.responseDescription = httpResponse.description
                    break
                default:
                    self.responseObjectWS!.responseDescription = "La petición no fue exitosa: \(httpResponse.description)"
                    self.responseObjectWS!.dataResponse = nil
                    break
                }
            }
            
            if let errorDetail  = error as NSError?{
                self.responseObjectWS!.responseDescription = error!.localizedDescription
                self.responseObjectWS!.dataResponse = nil
                self.responseObjectWS!.httpCode = errorDetail.code
            }
            
            if(self.cancelled){
                //Proceso cancelado:
                self.responseObjectWS!.responseDescription = "Connection canceled with error:\(error!.localizedDescription)"
                self.responseObjectWS!.httpCode = 0
                self.responseObjectWS!.code = self.SESSION_TRANSACTION_CANCELED
                self.responseObjectWS!.dataResponse = nil
            }
            
            
            self.delegate!.requestReturnedData(self.responseObjectWS!)
            
            
        })
        task.resume()
    }
    
}
