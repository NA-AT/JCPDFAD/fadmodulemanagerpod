//
//  FADModulePhoto.swift
//  FADModulePhoto
//
//  Created by Juan Carlos Pérez on 1/23/19.
//  Copyright © 2019 Juan Carlos Pérez. All rights reserved.
//

import UIKit
import FirebaseCore

public class FADModuleManager: NSObject {
    
    public static var initProcessOnSuccess : (()->Void)?
    public static var initProccessOnCancel : (()->Void)?
    public static var initProccessOnError : (()->Void)?
    
    
    public static var storyBoardNav : UINavigationController?

    
    public static func initProccess(navigation : UINavigationController?, constants:FADConstants, onSuccess:@escaping ()-> Void, onCancel:@escaping ()->Void, onError:@escaping ()-> Void) {
    
        FADModuleManager.registerFireBase()
        
        do {
            try Utils.fontsURLs().forEach({ try UIFont.register(from: $0) })
        } catch {
            print(error)
        }
        
        
        Constants.isSaveProcess = constants.isSaveProcess
        Constants.isProofLifeSelfie = constants.isProofLifeSelfie
        Constants.timeToTakeSelfie = constants.timeToTakeSelfie
        Constants.openEyesProbability = constants.openEyesProbability
        Constants.closeEyesProbability = constants.closeEyesProbability
        Constants.smileProbability = constants.smileProbability
        
        
        initProcessOnSuccess = onSuccess
        initProccessOnCancel = onCancel
        initProccessOnError = onError
        
        
        let storyboard = UIStoryboard(name: "Manager", bundle: Utils.getSDKBundle())
        let vc = storyboard.instantiateViewController(withIdentifier: "FADMainModuleViewControllerId") as! FADMainModuleViewController
        

        if let storyBoardNavigation = navigation{
            self.storyBoardNav = storyBoardNavigation
            
            //storyBoardNavigation.pushViewController(vc, animated: true)
            var arrayViewControllers = storyBoardNavigation.viewControllers
            arrayViewControllers.append(vc)
            
            //storyBoardNavigation.setViewControllers([vc], animated: true)
            storyBoardNavigation.setViewControllers([vc], animated: true)
            
            //window.rootViewController? = storyBoardNavigation
            //window.makeKeyAndVisible()
        }
        
    }
    
    
    public static func registerFireBase () {
       
        print("Configure SDK")
        
        if FirebaseApp.app() == nil {
            print("SDK APP ❌")
            
            let filePath = Utils.getSDKBundle().path(forResource: "GoogleService-Info", ofType: "plist")!
            let options = FirebaseOptions(contentsOfFile: filePath)
            FirebaseApp.configure(options: options!)
            
            print("SDK APP ✅")
            
        }else{
            print("SDK APP ✅")
        }
        
    }
    
}
