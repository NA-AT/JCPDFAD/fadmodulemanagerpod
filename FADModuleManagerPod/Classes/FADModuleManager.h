//
//  FADModuleManager.h
//  FADModuleManagerPod
//
//  Created by Juan Carlos Pérez on 2/7/19.
//


#import <UIKit/UIKit.h>

//! Project version number for FADModulePhoto.
FOUNDATION_EXPORT double FADModulePhotoVersionNumber;

//! Project version string for FADModulePhoto.
FOUNDATION_EXPORT const unsigned char FADModulePhotoVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <FADModulePhoto/PublicHeader.h>


//#import <FADModuleIdPod/FADModuleIdPod.h">
