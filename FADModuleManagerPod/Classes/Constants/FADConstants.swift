//
//  FADConstants.swift
//  FADModuleSelfiePod
//
//  Created by Juan Carlos Pérez on 2/7/19.
//

import UIKit


public class FADConstants: NSObject {
    
    open var endpoint_services : String = ""
    open var endpoint_oauth : String = ""
    open var available_free_space : Double = 0
    open var available_free_ram : Double = 0
    open var isSOCracked : Bool = false
    open var isLoadVerificationModule : Bool = false
    open var isLoadSignModule : Bool = false
    open var isSaveProcess : Bool = false
    open var isReadINE : Bool = false
    open var isReadPassPort : Bool = false
    open var isReadProofOfAddress : Bool = false
    open var isProofLifeSelfie : Bool = false
    open var timeout_request : Int = 0
    open var timeToTakeSelfie : Int = 0
    open var isLoadCurrentProcess : Bool = false
    open var isRemoveSavedData : Bool = false
    open var isEnableOAUTH : Bool = false
    
    open var pdfData : Data = Data()
    open var xmlData : Data = Data()
    open var imgCompany : UIImage?
    
    open var mainColor : UIColor?
    open var secondaryColor : UIColor?
    open var backgroundColor : UIColor?
    
    
    open var isAddMailScreen : Bool = false
    
    open var openEyesProbability : CGFloat = 0.85
    open var closeEyesProbability : CGFloat = 0.50
    open var smileProbability : CGFloat = 0.37
    
    open var isShowPrivacyView :  Bool = true
    
}
