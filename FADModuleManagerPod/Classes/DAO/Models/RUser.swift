//
//  RUser.swift
//  FADSelfieTest
//
//  Created by Juan Carlos Pérez on 2/5/19.
//  Copyright © 2019 Juan Carlos Pérez. All rights reserved.
//

import UIKit
import RealmSwift

class RUser: Object {
    @objc dynamic var id = 0
    @objc dynamic var selfie : Data?
    
    override static func primaryKey() -> String? {
        return "id"
    }
    
    
    //Incrementa ID
    func setNextId() -> Int{
        let realm = try! Realm()
        if let retNext = realm.objects(RUser.self).sorted(byKeyPath: "id").first?.id {
            return retNext + 1
        }else{
            return 1
        }
    }
}
