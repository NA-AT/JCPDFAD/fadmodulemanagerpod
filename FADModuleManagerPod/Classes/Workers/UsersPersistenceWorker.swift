//
//  UsersPersistenceManager.swift
//  AppSOFOM
//
//  Created by Juan Carlos Pérez D. on 1/1/18.
//  Copyright © 2018 Juan Carlos Pérez. All rights reserved.
//

import UIKit
import RealmSwift

class UsersPersistenceWorker: BaseDAO<RUser, NSString> {
    
    func saveRealmArray(_ objects: [RUser]) {
        let realm = try! Realm()
        try! realm.write {
            realm.add(objects, update: true)
            
        }
    }
    
}
